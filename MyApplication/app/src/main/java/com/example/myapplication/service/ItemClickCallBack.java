package com.example.myapplication.service;
import android.util.Log;

import com.example.myapplication.db.PointData;


public interface ItemClickCallBack {
    /**
     * 查看点击回调
     */
    void onCheckItemClicked(PointData data);

    /**
     * 删除点击回调
     */
    void onDeleteItemClicked(long id);
}
