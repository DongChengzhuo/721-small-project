package com.example.myapplication.util;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.myapplication.R;
import com.example.myapplication.db.PointData;
import com.example.myapplication.service.ItemClickCallBack;
import com.example.myapplication.service.PointDataService;

import java.util.List;
/**
 * 主界面适配器
 * @author: dongchengzhuo
 * @date: 2023/7/13
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private List<PointData> mPointDataList;
    private ItemClickCallBack callBack;
    private PointDataService mPointDataService = new PointDataService();

    public void setItemClickCallBack(@NonNull ItemClickCallBack callBack) {
        this.callBack = callBack;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        View pointDataView;
        TextView pointName;
        TextView northData;
        TextView eastData;
        TextView heightData;
        public ViewHolder(View itemView) {
            super(itemView);
            pointDataView = itemView;
            northData = itemView.findViewById(R.id.north_name);
            pointName = itemView.findViewById(R.id.point_name);
            heightData = itemView.findViewById(R.id.height_name);
            eastData = itemView.findViewById(R.id.east_name);
        }
    }

    public DataAdapter(List<PointData> pointDataList) {
        this.mPointDataList = pointDataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.pointDataView.setOnLongClickListener(v -> {
            int position = holder.getAbsoluteAdapterPosition();
            PointData pointData = mPointDataList.get(position);
            showPopMenu(v,pointData);
            return false;
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PointData rawData  = mPointDataList.get(position);
        holder.pointName.setText(rawData.getName());
        holder.eastData.setText(String.valueOf(rawData.getEast()));
        holder.northData.setText(String.valueOf(rawData.getNorth()));
        holder.heightData.setText(String.valueOf(rawData.getHeight()));
    }

    @Override
    public int getItemCount() {
        return mPointDataList.size();
    }

    public void showPopMenu(View view,PointData data){
        PopupMenu menu = new PopupMenu(view.getContext(), view);
        menu.getMenuInflater().inflate(R.menu.context,menu.getMenu());
        menu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()){
                case R.id.check_item:
                    callBack.onCheckItemClicked(data);
                    break;
                case R.id.delete_item:
                    callBack.onDeleteItemClicked(data.getId());
                    break;
            }
            return true;
        });
        menu.show();
    }

}
