package com.example.myapplication.db;

import androidx.annotation.NonNull;

public class DetailPointData {
    private String key;
    private String value;

    public DetailPointData(@NonNull String key, @NonNull String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(@NonNull String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(@NonNull String value) {
        this.value = value;
    }
}
