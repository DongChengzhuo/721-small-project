package com.example.myapplication;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.example.myapplication.db.PointData;
import com.example.myapplication.service.ItemClickCallBack;
import com.example.myapplication.service.PointDataService;
import com.example.myapplication.util.DataAdapter;
import org.litepal.tablemanager.Connector;
import java.util.List;

/**
 * 主界面实现
 * @author: dongchengzhuo
 * @date: 2023/7/13
 */
public class MainActivity extends AppCompatActivity {

    private PointDataService mPointDataService = new PointDataService();
    private ActivityResultLauncher mLauncher;
    private DataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            showRecyclerView();
            mLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == RESULT_OK){
                        showRecyclerView();
                    }
                }
            });
            adapter.setItemClickCallBack(new ItemClickCallBack() {
                @Override
                public void onCheckItemClicked(PointData data) {
                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                    intent.putExtra("pointData",data);
                    mLauncher.launch(intent);
                }

                @Override
                public void onDeleteItemClicked(long id) {
                    int rowsAffected = mPointDataService.deleteData(id);
                    if (rowsAffected == 1){
                        showRecyclerView();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *滚动数据显示
     */
    public void showRecyclerView(){
        List<PointData> pointDataList = mPointDataService.queryData();
        RecyclerView recyclerView = findViewById(R.id.recycler1_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        DataAdapter adapter = new DataAdapter(pointDataList);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.import_item:
                Toast.makeText(this,"导入",Toast.LENGTH_SHORT).show();
                break;
            case R.id.export_item:
                Toast.makeText(this,"导出",Toast.LENGTH_SHORT).show();
                break;

        }
        return true;
    }

    public void test(){

    }
}