package com.example.myapplication.service;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.myapplication.db.PointData;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;

import java.util.Arrays;
import java.util.List;

public class PointDataService implements PointDataManager{

    /**
     * 查询数据
     */
    @Override
    public List<PointData> queryData() {
        List<PointData> pointDataList = LitePal.findAll(PointData.class);
        return pointDataList;
    }


    /**
     * 根id据修改点名
     */
    @Override
    public int updateData(long id, @NonNull PointData pointData) {
        int rowsAffected = pointData.update(id);
        return rowsAffected;

    }

    @Override
    public int deleteData(long id) {
        int rowsAffected = LitePal.delete(PointData.class, id);
        return rowsAffected;
    }


    @Override
    public void insertData(@NonNull PointData pointData) {
        pointData.save();
    }
}
