package com.example.myapplication.service;

import com.example.myapplication.db.PointData;

import java.util.List;

public interface PointDataManager {
    /**
     * 查询主界面数据
     */
    List<PointData> queryData();


    /**
     * 根据修改数据
     */
    int updateData(long id,PointData pointData);

    /**
     * 删除数据
     */
    int deleteData(long id);

    /**
     * 增加数据
     */
    void insertData(PointData pointData);
}
