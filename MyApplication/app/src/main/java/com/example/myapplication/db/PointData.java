package com.example.myapplication.db;

import org.litepal.crud.LitePalSupport;

import java.io.Serializable;

public class PointData extends LitePalSupport implements Serializable {
    private long id;
    private String mName;
    private double mNorth;
    private double mEast;
    private double mHeight;
    private String mMeasureTime;
    private String mLongitude;
    private String mLatitude;
    private double mGeodeticHeight;
    private String mWgs84Longitude;
    private String mWgs84Latitude;

    public PointData() {
    }

    public PointData(String mName, double mNorth, double mEast, double mHeight, String mMeasureTime,
                     String mLongitude, String mLatitude, double mGeodeticHeight,
                     String mWgs84Longitude, String mWgs84Latitude) {
        this.mName = mName;
        this.mNorth = mNorth;
        this.mEast = mEast;
        this.mHeight = mHeight;
        this.mMeasureTime = mMeasureTime;
        this.mLongitude = mLongitude;
        this.mLatitude = mLatitude;
        this.mGeodeticHeight = mGeodeticHeight;
        this.mWgs84Longitude = mWgs84Longitude;
        this.mWgs84Latitude = mWgs84Latitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public double getNorth() {
        return mNorth;
    }

    public void setNorth(double mNorth) {
        this.mNorth = mNorth;
    }

    public double getEast() {
        return mEast;
    }

    public void setEast(double mEast) {
        this.mEast = mEast;
    }

    public double getHeight() {
        return mHeight;
    }

    public void setHeight(double mHeight) {
        this.mHeight = mHeight;
    }

    public String getMeasureTime() {
        return mMeasureTime;
    }

    public void setMeasureTime(String mMeasureTime) {
        this.mMeasureTime = mMeasureTime;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getGeodeticHeight() {
        return mGeodeticHeight;
    }

    public void setGeodeticHeight(double mGeodeticHeight) {
        this.mGeodeticHeight = mGeodeticHeight;
    }

    public String getWgs84Longitude() {
        return mWgs84Longitude;
    }

    public void setWgs84Longitude(String mWgs84Longitude) {
        this.mWgs84Longitude = mWgs84Longitude;
    }

    public String getWgs84Latitude() {
        return mWgs84Latitude;
    }

    public void setWgs84Latitude(String mWgs84Latitude) {
        this.mWgs84Latitude = mWgs84Latitude;
    }
}
