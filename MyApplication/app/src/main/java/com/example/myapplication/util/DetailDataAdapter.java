package com.example.myapplication.util;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.myapplication.R;
import com.example.myapplication.db.DetailPointData;
import java.util.List;

/**
 * 点详细界面查看
 * @author: dongchengzhuo
 * @date: 2023/7/14
 */
public class DetailDataAdapter extends RecyclerView.Adapter<DetailDataAdapter.ViewHolder> {
    private Context mContext;
    private List<DetailPointData> mPointDataList;

    public DetailDataAdapter(Context context, List<DetailPointData> mPointDataList) {
        this.mContext = context;
        this.mPointDataList = mPointDataList;
    }
    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView keyName;
        TextView valueName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            keyName = itemView.findViewById(R.id.keyName);
            valueName = itemView.findViewById(R.id.valueName);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.detail_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DetailPointData rawData = mPointDataList.get(position);
        holder.keyName.setText(rawData.getKey());
        holder.valueName.setText(rawData.getValue());
    }

    @Override
    public int getItemCount() {
        return mPointDataList.size();

    }
}
