package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.db.DetailPointData;
import com.example.myapplication.db.PointData;
import com.example.myapplication.service.PointDataService;
import com.example.myapplication.util.ConstructionList;
import com.example.myapplication.util.DetailDataAdapter;
import java.util.List;

/**
 * 点详细界面实现
 * @author: dongchengzhuo
 * @date: 2023/7/14
 */
public class DetailActivity extends AppCompatActivity {

    private PointDataService mPointDataService = new PointDataService();
    private PointData mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);
            Intent intent = getIntent();
            mData = (PointData) intent.getSerializableExtra("pointData");
            showRecyclerView();
            saveData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *滚动数据显示
     */
    public void showRecyclerView(){
        EditText mEditText = findViewById(R.id.edit_text);
        mEditText.setText(mData.getName());
        List<DetailPointData> detailPointDataList = new ConstructionList().constructionList(mData);
        RecyclerView recyclerView = findViewById(R.id.recycler2_view);
        LinearLayoutManager manager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(getDrawable(R.drawable.rv_item_divider));
        recyclerView.addItemDecoration(itemDecoration);
        DetailDataAdapter adapter = new DetailDataAdapter(this, detailPointDataList);
        recyclerView.setAdapter(adapter);
    }

    /**
     *保存修改数据
     */
    public void saveData(){
        EditText mEditText = findViewById(R.id.edit_text);
        Button button = findViewById(R.id.save_button);
        button.setOnClickListener(v -> {
            String inputText = mEditText.getText().toString();
            PointData pointData = new PointData();
            pointData.setName(inputText);
            if (mPointDataService.updateData(mData.getId(), pointData) == 1){
                Toast.makeText(this,"保存成功",Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(this,"保存失败",Toast.LENGTH_SHORT).show();
            }

            setResult(Activity.RESULT_OK);
        });

    }
}