package com.example.myapplication.util;

import androidx.annotation.NonNull;

import com.example.myapplication.db.DetailPointData;
import com.example.myapplication.db.PointData;

import java.util.ArrayList;
import java.util.List;
/**
 * 点详细数据结构转换
 * @author: dongchengzhuo
 * @date: 2023/7/14
 */
public class ConstructionList {
    public List<DetailPointData> constructionList(@NonNull PointData pointData){
        ArrayList<DetailPointData> detailPointDataList = new ArrayList<>();
        detailPointDataList.add( new DetailPointData("北",String.valueOf(pointData.getNorth())));
        detailPointDataList.add( new DetailPointData("东",String.valueOf(pointData.getEast())));
        detailPointDataList.add( new DetailPointData("高",String.valueOf(pointData.getHeight())));
        detailPointDataList.add( new DetailPointData("测量时间",pointData.getMeasureTime()));
        detailPointDataList.add( new DetailPointData("精度",pointData.getLongitude()));
        detailPointDataList.add( new DetailPointData("纬度",pointData.getLatitude()));
        detailPointDataList.add( new DetailPointData("大地高",String.valueOf(pointData.getGeodeticHeight())));
        detailPointDataList.add( new DetailPointData("wgs84经度",pointData.getWgs84Longitude()));
        detailPointDataList.add( new DetailPointData("wgs84纬度", pointData.getWgs84Latitude()));
        return detailPointDataList;
    }
}
