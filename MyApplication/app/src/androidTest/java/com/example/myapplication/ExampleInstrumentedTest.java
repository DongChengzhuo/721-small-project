package com.example.myapplication;

import android.content.Context;
import android.util.Log;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import com.example.myapplication.db.PointData;
import com.example.myapplication.service.PointDataService;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private PointDataService mPointDataService = new PointDataService();

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.myapplication", appContext.getPackageName());
        for (int i = 0; i < 1000; i++) {
            mPointDataService.insertData(new PointData("point2", 0.002, 0.006, 0.006, "sdss", "sss", "sssssaq", 0.054, "sdasd", "ssdsad"));
            Log.e("useAppContext: ", i + "");
        }
    }
}